BIN_PATH=/usr/local/bin/flextrack
SERVE_PATH=/root/flexget

.PHONY: remote_update
remote_update: update_bin update_serve 

.PHONY: update_bin
update_bin: dist/flextrack
	scp $^ root@flexget:$(BIN_PATH)

.PHONY: update_serve
update_serve: extra/schema.sql extra/series.template extra/serve.py
	scp $^ root@flexget:/root/flextrack/
