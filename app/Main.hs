{-# LANGUAGE OverloadedStrings #-}
module Main where

import Control.Applicative
import Control.Monad
import Data.Time
import Data.Time.Clock
import qualified Data.Map.Lazy
import Database.SQLite.Simple
import Database.SQLite.Simple.FromRow
import Database.SQLite.Simple.Time
import System.Console.ArgParser

dbname :: String
dbname = "/root/.flexget/stat/flextrack.db"

data ShowRow = ShowRow { downloadID :: Int
                       , downloadName :: String
                       , downloadUrl :: String
                       , downloadTime :: UTCTime
                       , downloadStatus :: String
                       }

-- Container for querying sqlite results()
data NResults = NResults Int
                deriving (Show, Eq)

noChanges :: NResults
noChanges = NResults 0

instance FromRow ShowRow where
    fromRow = ShowRow <$> field <*> field <*> field <*> field <*> field

instance FromRow NResults where
    fromRow = NResults <$> field

instance Show ShowRow where
    show s = (show . downloadID) s ++ "|"
             ++ (show . downloadTime) s ++ "|"
             ++ downloadStatus s ++ "|"
             ++ downloadName s ++ "|"
             ++ downloadUrl s

addNewShow :: String -> String -> String -> IO ()
addNewShow t u s = do
    conn <- open dbname
    execute conn "INSERT INTO shows (title, url, status) VALUES (?, ?, ?)" (t :: String, u :: String, s :: String) 
    close conn

anyResults :: Connection -> IO Bool
anyResults c = liftM ((noChanges /=) . head) (query_ c "SELECT changes()" :: IO [NResults])

updateStatus :: String -> String -> IO ()
updateStatus t s = do
    conn <- open dbname
    execute conn "UPDATE shows SET status=? WHERE title=?" (s :: String, t :: String)
    wereResults <- anyResults conn
    unless wereResults $ execute conn "INSERT INTO shows (title, url, status) VALUES (?, ?, ?)"
                             (t :: String, "#" :: String, "orphan" :: String)
    close conn

dumpShows :: IO ()
dumpShows = do
    conn <- open dbname
    r <- query_ conn "SELECT * FROM shows ORDER BY time DESC" :: IO [ShowRow]
    mapM_ print r
    close conn

getDiffDay :: Int -> IO UTCTime
getDiffDay n = do
    ctime <- getCurrentTime
    let cday = addDays (- fromIntegral n) (utctDay ctime)
    return $ UTCTime cday (utctDayTime ctime)

purgeShows :: Int -> IO ()
purgeShows n = do
    d <- getDiffDay n
    conn <- open dbname
    execute conn "DELETE FROM shows WHERE time < ?" (Only d)
    execute conn "DELETE FROM runs WHERE time < ?" (Only d)
    close conn

rmShow :: Int -> IO ()
rmShow i = do
    conn <- open dbname
    execute conn "DELETE FROM shows WHERE id IS ? " (Only $ show i)
    close conn

registerRun :: String -> IO ()
registerRun t = do
    conn <- open dbname
    execute conn "INSERT INTO runs (task_name) VALUES (?)" (Only t)
    close conn

data CliInterface  = 
    AddParams { newName :: String, newUrl :: String, newAddStatus :: String } |
    UpdateParams { updateName :: String, newStatus :: String } |
    PurgeParams { nDays :: Int } |
    DeleteParams { showId :: Int } |
    RegisterParams { taskName :: String } |
    PrintShows
    deriving (Show)

nullArgs :: a -> String -> CmdLnInterface a
nullArgs t s = let parseResult = Right t
                   niceArgs = ([], Data.Map.Lazy.empty)
                   parser = Parser $ const (parseResult, niceArgs)
                   paramDescr = ParamDescr id "" id "" ""
                   parserSpec = ParserSpec [paramDescr] parser
               in CmdLnInterface parserSpec [] s (Just "") (Just "") (Just "")

cliParser :: IO (CmdLnInterface CliInterface)
cliParser = mkSubParser
    [ ("add", mkDefaultApp
      (AddParams `parsedBy` reqPos "name" `andBy` reqPos "url" `andBy` optPos "new" "status") "add")
    , ("update", mkDefaultApp
      (UpdateParams `parsedBy` reqPos "name" `andBy` reqPos "status") "update")
    , ("purge", mkDefaultApp
      (PurgeParams `parsedBy` optPos 3 "days") "purge")
    , ("delete", mkDefaultApp
      (DeleteParams `parsedBy` reqPos "id") "delete")
    , ("register", mkDefaultApp
      (RegisterParams `parsedBy` reqPos "task") "register")
    , ("print", nullArgs PrintShows "print")
    ]

doMain :: CliInterface ->  IO ()
doMain c = case c of AddParams t u s -> addNewShow t u s
                     UpdateParams t s -> updateStatus t s
                     PrintShows -> dumpShows
                     PurgeParams n -> purgeShows n
                     DeleteParams i -> rmShow i
                     RegisterParams t -> registerRun t

main :: IO ()
main = do
    interface <- cliParser
    runApp interface doMain
