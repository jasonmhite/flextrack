CREATE TABLE IF NOT EXISTS shows (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    title STRING NOT NULL,
    url STRING NOT NULL,
    time TIME DEFAULT (CURRENT_TIMESTAMP),
    status STRING NOT NULL
);
CREATE TABLE IF NOT EXISTS runs (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    time TIME DEFAULT (CURRENT_TIMESTAMP),
    task_name STRING NOT NULL
);
