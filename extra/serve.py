import sqlite3
from jinja2 import Template
from flask import Flask
import tvnamer
from tvnamer.utils import FileParser

import datetime
from datetime import timezone

TEMPLATE_PATH = '/root/flextrack/series.template'
TIME_FMT = '%a %b %d %H:%M:%S %Y'
DBFILE = "/root/.flexget/stat/flextrack.db"

with open(TEMPLATE_PATH) as f:
    template = Template(f.read())

app = Flask(__name__)

con = sqlite3.connect(DBFILE)

def utc_to_local(utc_time):
    return utc_time.replace(tzinfo=timezone.utc).astimezone(tz=None)

def read_error(s):
    if "new" in s:
        return ""
    elif "error" in s:
        return "list-group-item-danger"
    elif "orphan"in s:
        return "list-group-item-info"
    elif "done" in s:
        return "list-group-item-success"

def fetch_rows():
    with con:
        cursor = con.execute("SELECT * FROM shows ORDER BY time DESC")

        data = []
        for r in cursor:
            entry = {
                "filename": r[1],
                "url": r[2],
                "utctime": r[3],
                "status": r[4],
                "class": read_error(r[4])
            }

            localtime = utc_to_local(
                datetime.datetime.strptime(
                    entry['utctime'],
                    "%Y-%m-%d %H:%M:%S"
                )
            ).strftime(TIME_FMT)
            entry['time'] = localtime

            try:
                p = FileParser(entry['filename']).parse().getepdata()
                entry['title'] = "{} - {}".format(p['seriesname'], p['episode'])
            except tvnamer.tvnamer_exceptions.InvalidFilename:
                entry['title'] = entry['filename']
                entry['class'] = "list-group-item-warning"

            data.append(entry)

        return data

# Need to have flexget write its runtimes for this
def get_latest_run():
    with con:
        cursor = con.execute("SELECT time FROM runs ORDER BY time DESC LIMIT 1")
        entries = list(cursor)

        if len(entries) != 1:
            localtime = "Error reading time"
        else:
            time = entries[0][0]

        localtime = utc_to_local(
            datetime.datetime.strptime(
                time,
                "%Y-%m-%d %H:%M:%S"
            )
        ).strftime(TIME_FMT)

    return localtime

@app.route("/")
def main():
    shows = fetch_rows()
    # now = utc_to_local(datetime.datetime.now()).strftime(TIME_FMT)
    now = get_latest_run()

    return template.render(shows=shows, now=now)


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=80)
